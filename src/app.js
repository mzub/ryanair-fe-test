import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import Components from './components/';
import Pages from './pages/';
import config from './app.config';
import AirportsService from './services/airports.service';
import CheapFlightService from './services/cheapflights.service';
import {
  APP_NAME
} from './constants';
import './scss/loader.scss';
import './scss/main.scss';

const requires = [
  uiRouter,
  Components,
  Pages
];

angular.module(APP_NAME, requires)
  .service('AiportsService', AirportsService)
  .service('CheapFlightService', CheapFlightService)
  .config(config);

angular.bootstrap(document, [APP_NAME], {
  strictDi: true
});
