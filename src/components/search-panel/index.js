import angular from 'angular';
import SearchPanelComponent from './search-panel.component';
import './search-panel.scss';

const searchPanel = angular
  .module('searchPanel', [])
  .component('searchPanel', SearchPanelComponent)
  .name;

export default searchPanel;
