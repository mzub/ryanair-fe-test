import SearchPanelController from './search-panel.controller';
import teamplate from './search-panel.html';

const SearchPanel = {
  template: teamplate,
  controller: SearchPanelController,
  controllerAs: 'vm',
};

export default SearchPanel;
