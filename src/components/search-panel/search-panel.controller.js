import moment from 'moment';

const DATE_FORMAT_API = 'YYYY-MM-DD';
const formatDate = d => moment(d, DATE_FORMAT_API).toDate();

const pickAirportByIATACode = (airports, code) => airports.find(airport => airport.iataCode === code);

class FlightList {
  constructor(CheapFlightService, AiportsService, $state) {
    'ngInject';

    this.AiportsService = AiportsService;
    this.$state = $state;
    this.loader = true;
  }

  $onInit() {
    this.AiportsService.getAirports().then((resp) => {
      this.options = resp;
      this.loader = false;

      // Init data from params
      const params = this.$state ? this.$state.params : null;
      if (params) {
        this.startDate = params.flyOut ? formatDate(params.flyOut) : null;
        this.endDate = params.flyBack ? formatDate(params.flyBack) : null;
        this.airportFrom = params.airportFrom ? pickAirportByIATACode(this.options.airports, params.airportFrom) : null;

        const airportTo = params.airportTo ? pickAirportByIATACode(this.options.airports, params.airportTo) : null;
        this.allowedRoutesTo = this.options.routes[params.airportFrom];
        if (airportTo && airportTo.iataCode) {
          this.airportTo = this.allowedRoutesTo.includes(airportTo.iataCode) ? airportTo : null;
        }
      }
    });
  }

  submit() {
    if (this.formSearch.$valid &&
      this.airportTo &&
      this.airportFrom &&
      moment(this.startDate).isValid() &&
      moment(this.endDate).isValid()) {
      const sendObj = {
        airportFrom: this.airportFrom.iataCode ? this.airportFrom.iataCode : null,
        airportTo: this.airportTo.iataCode ? this.airportTo.iataCode : null,
        flyOut: moment(this.startDate).format(DATE_FORMAT_API).valueOf(),
        flyBack: moment(this.endDate).format(DATE_FORMAT_API).valueOf(),
      };
      this.formSearch.$setPristine();

      // flights state resolve data from params
      this.$state.go('home.flights', sendObj, {
        reload: false,
        notify: true,
      });
    }
  }
  // Airports
  changeAirportFrom(airport) {
    this.airportFrom = airport;
    if (airport) {
      this.openPopupAirportFrom();
      this.allowedRoutesTo = this.options.routes[airport.iataCode];

      // Check toutes
      if (this.allowedRoutesTo && this.airportTo) {
        if (!this.allowedRoutesTo.includes(this.airportTo.iataCode)) {
          this.airportTo = null;
          this.openPopupAirportTo();
        }
      }
    } else {
      this.allowedRoutesTo = [];
    }
  }

  changeAirportTo(airport) {
    this.airportTo = airport;
    if (airport) {
      this.openPopupAirportTo();
    }
  }

  openPopupAirportFrom() {
    this.isOpenAirportFrom = false;
    if (!this.airportTo) {
      this.isOpenAirportTo = true;
    }
  }

  openPopupAirportTo() {
    // Toggle Airport To
    this.isOpenAirportTo = false;
    if (!this.airportFrom) {
      this.isOpenAirportFrom = true;
    }
  }

  // Dates
  changeStartDate(date) {
    this.startDate = date;
    if (date && this.endDate && moment(this.startDate) > moment(this.endDate)) {
      this.endDate = moment(date).add(2, 'd').toDate();
    }
  }

  changeEndDate(date) {
    this.endDate = date;
    if (date && this.startDate && moment(this.endDate) < moment(this.startDate)) {
      this.startDate = moment(date).subtract(2, 'd').toDate();
    }
  }

  // TODO: try to use ng-blur in airport-selector.controller
  onFocusHandler(element) {
    if (element.name === 'from') {
      this.isOpenAirportFrom = true;
      this.isOpenAirportTo = false;
    } else if (element.name === 'to') {
      this.isOpenAirportFrom = false;
      this.isOpenAirportTo = true;
    }
  }
}


export default FlightList;
