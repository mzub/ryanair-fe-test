const pickAllowedCountries = (countries, allowed) => countries.map((country) => {
  const newCountry = country;
  newCountry.disabled = allowed.indexOf(country.code) === -1;
  return newCountry;
});

const filterAllowedCountries = airports => airports
  .filter(airport => !airport.disabled)
  .map(airport => airport.country.code);

const filterDisabledAirports = (airports, allowed) => airports.map((airport) => {
  const newAirort = airport;
  newAirort.disabled = allowed.indexOf(newAirort.iataCode) === -1;
  return newAirort;
});

const clearActive = list => list.map((item) => {
  const newItem = item;
  newItem.active = false;
  return newItem;
});

const filterCountries = (countries, airports, allowedAirports) => {
  const filteredDisabledAirports = filterDisabledAirports(airports, allowedAirports);
  const allowedCountries = filterAllowedCountries(filteredDisabledAirports);
  const filterDisabledCountries = (allowedCountries && allowedCountries.length) ?
    pickAllowedCountries(countries, allowedCountries) : countries;
  return {
    countries: clearActive(filterDisabledCountries),
    airports: filteredDisabledAirports
  };
};

const filteredAirportsByCountry = (airports, countryCode) =>
  airports.filter(airport => airport.country.code === countryCode);

const findCountryIndex = (countries, code) => countries.findIndex(country => country.code === code);

const resetPrevActiveItem = (list) => {
  const listCopy = angular.copy(list);
  const prevActiveIndex = list.findIndex(item => item.active === true);
  if (prevActiveIndex >= 0) {
    listCopy[prevActiveIndex].active = false;
  }
  return listCopy;
};

class CustomSelectorController {
  constructor($element, $document, $scope) {
    'ngInject';

    this.$element = $element;
    this.$document = $document;
    this.$scope = $scope;
  }

  $onInit() {
    this.isOpen = false;
  }

  $postLink() {
    // TOOD: outside click
    // find betetr solution
    const el = this.$element;
    this.$document.on('click', (e) => {
      if (this.isOpen && el !== e.target && !el[0].contains(e.target)) {
        this.isOpen = false;
        this.$scope.$applyAsync();
      }
    });
  }

  $onChanges(changes) {
    if (changes.options &&
      !angular.equals(changes.options.currentValue, changes.options.previousValue)) {
      this.handleOptionsChanges(angular.copy(changes.options.currentValue));
    }

    // Toggle dropdown content from parent
    if (typeof changes.isOpen !== 'undefined' &&
      changes.isOpen.currentValue !== changes.isOpen.previousValue) {
      this.isOpen = changes.isOpen.currentValue;
    }

    // Change from parent selected
    if (changes.selected &&
      !angular.equals(changes.selected.currentValue, changes.selected.previousValue)) {
      const currentValue = changes.selected.currentValue;
      if (currentValue && currentValue.name && currentValue.iataCode) {
        this.selected = angular.copy(currentValue);
      }
    }

    // Allowed airpots
    if (changes.allowedAirports &&
      !angular.equals(changes.allowedAirports.currentValue, changes.allowedAirports.previousValue)) {
      if (changes.allowedAirports.currentValue) {
        const result = filterCountries(this.countries, this.airports, changes.allowedAirports.currentValue);
        this.handleOptionsChanges(result);
      }
    }
  }

  handleOptionsChanges(options) {
    if (options) {
      this.countries = options.countries ? options.countries : null;
      this.airports = options.airports ? options.airports : null;

      if (this.selected && this.selected.iataCode) {
        const countryIndex = findCountryIndex(this.countries, this.selected.country.code);
        if (countryIndex >= 0) {
          this.countries[countryIndex].active = true;
          this.airportsDisplay = filteredAirportsByCountry(this.airports, this.selected.country.code) || [];
        } else {
          this.airportsDisplay = [];
        }
      }
    }
  }

  changeAirport() {
    const search = this.selected && this.selected.name ? this.selected.name.toLowerCase() : null;
    if (search && search.length) {
      this.isOpen = true;
    }

    // Filter Countries by search value
    this.countries = this.countries.map((country) => {
      const name = country.name ? country.name.toLowerCase() : null;
      // Can be used {...country} but need some conficurations for eslint
      const newCountry = Object.assign({}, country);
      newCountry.isHighlighted = search && name.indexOf(search) >= 0;
      return newCountry;
    });

    // Filter airports by search value
    this.airportsDisplay = this.airports.filter((airport) => {
      const name = airport.name ? airport.name.toLowerCase() : null;
      return search && name && name.indexOf(search) >= 0;
    });

    // set empty value
    if (!search) {
      this.selected = null;
      this.onChange({
        airport: this.selected
      });
    }

    // Remove prev active country highlight
    this.countries = clearActive(this.countries);
  }

  selectCountry(index, $event) {
    const country = this.countries[index];
    const countries = resetPrevActiveItem(this.countries);
    countries[index].active = true;
    this.countries = countries;

    this.airportsDisplay = filteredAirportsByCountry(this.airports, country.code) || [];
    this.isOpen = true;
    $event.stopPropagation();
  }

  selectAirport(airport, $event) {
    if (airport) {
      this.selected = airport;
      this.isOpen = false;
      this.onChange({
        airport
      });
    }
    this.countries = this.countries.map((country) => {
      const newCountry = Object.assign({}, country);
      newCountry.isHighlighted = false;
      return newCountry;
    });
    $event.stopPropagation();
  }

  onFocus() {
    this.isOpen = true;
    const element = {
      isOpen: this.isOpen,
      name: this.name
    };
    this.onFocusEvent({
      element
    });
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }
}

export default CustomSelectorController;
