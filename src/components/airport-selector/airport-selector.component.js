import AirportSelectorController from './airport-selector.controller';
import template from './airport-selector.html';

const airportSelector = {
  bindings: {
    options: '<',
    name: '@',
    label: '@',
    required: '@',
    placeHolder: '@',
    isOpen: '<',
    onChange: '&',
    onFocusEvent: '&',
    selected: '<',
    className: '@',
    allowedAirports: '<'
  },
  template,
  controller: AirportSelectorController,
  controllerAs: 'vm',
};

export default airportSelector;
