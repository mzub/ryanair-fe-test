import angular from 'angular';
import AirportSelectorComponent from './airport-selector.component';
import './airport-selector.scss';

const customSelector = angular
  .module('airportSelector', [])
  .component('airportSelector', AirportSelectorComponent)
  .name;

export default customSelector;
