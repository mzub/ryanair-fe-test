import angular from 'angular';
import flightList from './flight-list/';
import airportSelector from './airport-selector/';
import searchPanel from './search-panel/';
import datepicker from './datepicker/';

const requires = [
  flightList,
  airportSelector,
  searchPanel,
  datepicker
];

export default angular.module('app.components', requires)
  .name;
