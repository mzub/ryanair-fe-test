import angular from 'angular';
import FlightListComponent from './flight-list.component';
import './flight-list.scss';

const flightList = angular
  .module('flightList', [])
  .component('flightList', FlightListComponent)
  .name;

export default flightList;
