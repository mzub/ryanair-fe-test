import {
  priceToUi,
  dateToUI
} from '../../utils';

const flyToUI = item => ({
  currency: item.currency,
  price: priceToUi(item.price),
  dateFrom: dateToUI(item.dateFrom),
  dateTo: dateToUI(item.dateTo)
});

const prepareFlightList = list => list.map(item => flyToUI(item));

class FlightList {
  constructor() {
    'ngInject';
  }

  $onChanges(changes) {
    if (changes.flyghts &&
      !angular.equals(changes.flyghts.currentValue, changes.flyghts.previousValue)) {
      const currentValue = changes.flyghts.currentValue;
      this.flyghts = prepareFlightList(currentValue);
      this.showList = currentValue && currentValue.length;
    }
  }

}

export default FlightList;
