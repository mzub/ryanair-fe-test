import FlightList from './flight-list.controller';
import teamplate from './flight-list.html';

const flightList = {
  bindings: {
    flyghts: '<'
  },
  template: teamplate,
  controller: FlightList,
  controllerAs: 'vm',
};

export default flightList;
