class DatepickerController {
  constructor() {
    'ngInject';
  }

  $onChanges(changes) {
    if (changes.dateValue &&
      !angular.equals(changes.dateValue.currentValue, changes.dateValue.previousValue)) {
      this.dateValue = angular.copy(changes.dateValue.currentValue);
    }
  }

  changeDate() {
    this.onChange({
      date: this.dateValue
    });
  }

}

export default DatepickerController;
