import angular from 'angular';
import DatepickerComponent from './datepicker.component';

const datepicker = angular
  .module('datepicker', [])
  .component('datepicker', DatepickerComponent)
  .name;

export default datepicker;
