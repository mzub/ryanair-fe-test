import DatepickerController from './datepicker.controller';
import teamplate from './datepicker.html';

const datepicker = {
  bindings: {
    dateValue: '<',
    name: '@',
    label: '@',
    required: '@',
    placeHolder: '@',
    onChange: '&'
  },
  template: teamplate,
  controller: DatepickerController,
  controllerAs: 'vm',
};

export default datepicker;
