import {
  API_URL
} from '../constants';

class AiportsService {
  constructor($http) {
    this.$http = $http;
  }

  getAirports() {
    return this.$http.get(`${API_URL}/forms/flight-booking-selector/`).then(response => response.data);
  }
}

AiportsService.$inject = ['$http'];

export default AiportsService;
