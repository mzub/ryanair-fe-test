import {
  API_URL
} from '../constants';

class CheapFlightService {
  constructor($http) {
    this.$http = $http;
  }

  search(params) {
    let result;
    if (params.airportFrom &&
      params.airportTo &&
      params.flyOut &&
      params.flyBack) {
      const urlParams = `${params.airportFrom}/to/${params.to}/${params.flyOut}/${params.flyBack}`;
      const url = `/flights/from/${urlParams}/250/unique/?limit=15&offset-0`;
      result = this.$http.get(API_URL + url).then(response => response.data);
    } else {
      result = [{
        code: '-1',
        message: 'Please provide params'
      }];
    }
    return result;
  }
}

CheapFlightService.$inject = ['$http'];

export default CheapFlightService;
