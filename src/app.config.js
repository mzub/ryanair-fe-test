function routes($urlRouterProvider, $locationProvider, $urlMatcherFactoryProvider) {
  $urlRouterProvider.otherwise('/');

  $urlMatcherFactoryProvider.strictMode(true);

  // $locationProvider.html5Mode({
  //     enabled: true,
  // });

  // $locationProvider.hashPrefix('!');
}

routes.$inject = ['$urlRouterProvider', '$locationProvider', '$urlMatcherFactoryProvider'];

export default routes;
