import Flight from './flights.controller';
import teamplate from './flights.html';

const flight = {
  bindings: {
    flights: '<'
  },
  template: teamplate,
  controller: Flight,
  controllerAs: 'vm',
};

export default flight;
