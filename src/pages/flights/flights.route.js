const flightsResolver = ($transition$, CheapFlightService) => {
  const params = $transition$.params();
  return CheapFlightService.search(params);
};

function routes($stateProvider) {
  $stateProvider
    .state('home.flights', {
      url: 'flights/from:airportFrom-to:airportTo/:flyOut/:flyBack',
      component: 'flights',
      resolve: {
        flights: flightsResolver
      }
    });
}

routes.$inject = ['$stateProvider'];

flightsResolver.$inject = ['$transition$', 'CheapFlightService'];

export default routes;
