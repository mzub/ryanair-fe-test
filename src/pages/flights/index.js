import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import routes from './flights.route';
import FlightsComponent from './flights.component';

export default angular
  .module('flights', [uiRouter])
  .component('flights', FlightsComponent)
  .config(routes)
  .name;
