class FlightsController {
  constructor() {
    'ngInject';
  }

  $onChanges(changes) {
    if (changes.flights &&
      !angular.equals(changes.flights.currentValue, changes.flights.previousValue)) {
      this.flights = changes.flights.currentValue.flights;
    }
  }
}

export default FlightsController;
