import template from './home.html';

function routes($stateProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      views: {
        '@': {
          template
        }
      },
    });
}

routes.$inject = ['$stateProvider'];

export default routes;
