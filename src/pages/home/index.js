import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import route from './home.route';
import HomeComponent from './home.component';

export default angular
  .module('home', [uiRouter])
  .component('homePage', HomeComponent)
  .config(route)
  .name;
