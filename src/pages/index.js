import Home from './home/';
import Flights from './flights/';
import Task from './task/';

const require = [
  Home,
  Flights,
  Task,
];

const pages = angular.module('app.pages', require)
  .name;

export default pages;
