function routes($stateProvider) {
  $stateProvider
    .state('task', {
      url: '/task',
      views: {
        '@': {
          component: 'task'
        }
      },
    });
}

routes.$inject = ['$stateProvider'];

export default routes;
