import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import route from './task.route';
import TaskComponent from './task.component';

export default angular
  .module('app.pages.task', [uiRouter])
  .component('task', TaskComponent)
  .config(route)
  .name;
