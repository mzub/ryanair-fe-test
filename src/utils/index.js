import moment from 'moment';

export const dateToUI = date => moment(date).format('DD/MM/YYYY');
// dateToApi

export const priceToUi = price => price.toFixed(2);
